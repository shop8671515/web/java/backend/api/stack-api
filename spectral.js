const ibmRuleset = require('@ibm-cloud/openapi-ruleset');
const { propertyCaseConvention } = require('@ibm-cloud/openapi-ruleset/src/functions');

module.exports = {
    extends: [ibmRuleset],
    rules: {
        'property-case-convention': {
            given: [
                '$.paths[*][parameters][*].schema',
                '$.paths[*][parameters][*].content[*].schema',
                '$.paths[*][*][requestBody].content[*].schema'
            ],
            then: {
                function: propertyCaseConvention,
                functionOptions: {
                    type: 'camel'
                }
            }
        }
    },
};
