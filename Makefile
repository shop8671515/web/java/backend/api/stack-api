.ONESHELL:
SHELL := bash
.SHELLFLAGS = -ceuo pipefail
POSTFIX := .openapi.yml
OPENAPI_MAKEFILE := /opt/makefile/Makefile
FILES = $(wildcard src/*$(POSTFIX))
VERSIONS = $(FILES:src/%$(POSTFIX)=src/%.VERSION)
DOCKER_IMAGE_VERSION := 6
DOCKER_IMAGE := bananawhite98/openapi:0.0.36
PY := python3

versions: $(VERSIONS)
	@for path in $^; do
		file=$$(basename $$path)
		@echo "$${file::-8}: $$(cat $$path)";
	@done

libs-npm: $(VERSIONS)
	@for path in $^; do
		file=$$(basename $$path)
		@echo "npm install --save @stack/$${file::-8}@$$(cat $$path)";
	@done

libs-mvn-feign:
	@make .deps-mvn ARTIFACT=feign

libs-mvn-spring:
	@make .deps-mvn ARTIFACT=api

ci-push-java:
	set -e
	SPEC="./src/$(MODULE)$(POSTFIX)"
	MVN_OPTS="-s $${CI_PROJECT_DIR}/.mvn/settings.xml"; \
	if [[ ! -v CI ]]; then
		MVN_OPTS="-Duser.home=$${HOME}"; \
	fi
	make -f $(OPENAPI_MAKEFILE) \
		OPENAPI_DIR=. \
		FILES_POSTFIX=$(POSTFIX) \
		FILES="$${SPEC}" \
		MVN_OPTS="$${MVN_OPTS}" \
		push-$(VARIANT)

ci-gen-java: src/$(MODULE).VERSION
	set -e
	SPEC="./src/$(MODULE)$(POSTFIX)"
	MVN_OPTS="-s $${CI_PROJECT_DIR}/.mvn/settings.xml"; \
	if [[ ! -v CI ]]; then
		MVN_OPTS="-Duser.home=$${HOME}"; \
	fi
	make -f $(OPENAPI_MAKEFILE) \
		OPENAPI_DIR=. \
	        OPENAPI_PARAMS="--template-dir config/templates/java/$(VARIANT)/" \
		FILES_POSTFIX=$(POSTFIX) \
		FILES="$${SPEC}" \
		MVN_OPTS="$${MVN_OPTS}" \
		$(VARIANT)-api

ci-gen-ts: src/$(MODULE).VERSION
	SPEC="./src/$${MODULE}$(POSTFIX)"
	make -f $(OPENAPI_MAKEFILE) \
		OPENAPI_DIR=. \
		OPENAPI_PARAMS='--template-dir config/templates/typescript/' \
		FILES_POSTFIX=$(POSTFIX) \
		FILES="$${SPEC}" \
		typescript-api

ci-push-ts:
	echo "@stack:registry=https://registry.npmjs.org/" >| /root/.npmrc
	echo "//registry.npmjs.org/:_authToken=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" >> /root/.npmrc
	SPEC="./src/$${MODULE}$(POSTFIX)"
	make -f $(OPENAPI_MAKEFILE) \
		OPENAPI_DIR=. \
		FILES_POSTFIX=$(POSTFIX) \
		FILES="$${SPEC}" \
		typescript-api push-typescript